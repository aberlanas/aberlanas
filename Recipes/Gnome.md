# Gnome Recipes

## Using kgx under Gnome

KGX is a new Gnome App for Terminal 

```shell
sudo apt install kgx
gsettings set org.gnome.desktop.default-applications.terminal exec 'kgx'
sudo update-alternatives --config x-terminal-emulator
```

## For disable bell

```shell
gsettings set org.gnome.desktop.wm.preferences audible-bell false
gsettings set org.gnome.desktop.wm.preferences visual-bell false
```



