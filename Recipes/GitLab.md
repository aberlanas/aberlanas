# Tricks for GitLab

 * Go to project directory in terminal using cd path/to/project
 * Run `ssh-keygen` 
 * Press enter for passphrase
 * Run `cat ~/.ssh/id_rsa.pub` in terminal
 * Copy the key that you get at the terminal
 * Go to **Gitlab/Settings/SSH-KEYS**
 * Paste the key and press Add Key button


