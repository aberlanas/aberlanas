#!/usr/bin/make -f 


prepare-dirs: 
	@echo " * [ Building directories ] "
	mkdir -p /home/aberlanas/bin/


install : install-dotfiles

	@echo " * Dummy for now "

emacs :
	@echo " * .emacs folder "
	mkdir -p /home/aberlanas/.emacs.d/
	mkdir -p /home/aberlanas/.emacs.d/themes/
	cp dotfiles/zenburn-theme.el  /home/aberlanas/.emacs.d/themes/
	cp dotfiles/init.el /home/aberlanas/.emacs.d/

install-bashrc:

	@echo " * [ Copying Bashrc config ]"
	cp dotfiles/dot.bashrc /home/aberlanas/.bashrc

install-dotfiles: install-bashrc

	@echo " * [ Copying configs ]"
