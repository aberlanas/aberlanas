#+Title: Acta Reunión
#+Author: Comisión PEAC

* 

* Peticiones Hardware

  | Cantidad | Item |
  |----------+------|
  |10 |Discos Duros|
  |7  |MicroSDs|
  |15 |Hubs usb|
  |3  |Webcams|
  |7 |Conversores HDMI a DVI/VGA|
  |5 |Ventiladores USB|
  |5 |Packs de destornillador|
  |7 |Raspberry|
  |1 |Compresor|
