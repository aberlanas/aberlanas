
# Ejercicios de entrenamiento ProgramaMe 2022

Actualizado a Enero 2022

## Sencillos

![Easy](imgs/easy-easy.png)\

| Número  | Título  | Temario| 
|---------|---------|--------|
| 116 |¡Hola mundo! | Introducción a Acepta El Reto!|
| 117 | La fiesta aburrida | Cadenas de texto (String).|
| 149 | San Fermines | Condicionales simples. Máximo.|
| 155 | Perímetro de un rectángulo |Expresiones.|
| 180 | Triángulos| Condicionales.|
| 217 | ¿Qué lado de la calle? |Condicionales simples. Par / Impar.|
| 293 | Artrópodos |Expresiones.|
| 313 | Fin de mes |Expresiones.|
| 362 | El día de Navidad |Condicionales simples.|
| 363 | Las calorias |Expresiones.|
| 368 | Cociendo Huevos| Expresiones.|
| 369 | Contando en la arena |Cadenas de texto (String).|
| 371 | Aburrimiento en las sobremesas |Expresiones.|
| 380 | ¡Me caso!| Suma de variables.|
| 413 | Claras y oscuras |Expresiones.|
| 427 | Yo soy tu... |Cadenas de texto (String).|
| 434 | Romance en el palomar| Condicionales simples.|
| 456 | Tarta Sacher| Expresiones.|
| 467 | Polisílaba es polisílaba| Cadenas de texto (String).|
| 140 | Suma de dígitos | 3433 → 3 + 4 + 3 + 3 = 13|
| 151 | ¿Es matriz identidad? | Matrices.|
| 166 | Zapping Lista circular de enteros. |Calcular diferencia.|
| 171 | Abadı́as pirenáicas |Vectores.|
| 180 | Triángulos |Condicionales.|
| 216 | Goteras |Pasar de segundos a horas, minutos y segundos.|
| 219 | La lotería de la peña Atlética| Vectores.|
| 252 | ¿Acaso hubo búhos acá? |Cadenas de texto (String).|
| 337 | La abuela Marı́a |Vectores.|
| 359 | Timo en el cocedero de mariscos |Expresiones / Bucles simples.|
| 370 | La 13-14 Condicionales. |Tratamiento de la entrada (split).|
| 374 | Criogenización |Bucles simples. Máximo, mínimo y contar.|
| 407 | Rebotando en el parchís |Expresiones.|
| 411 | Sobre la tela de una araña |Bucles simples.|
| 416 | Michael J. Fox y el Pato Donald |Bucles anidados.|
| 435 | El Pijote |Vectores.|
| 446 | Abuelas falsas |Bucles simples. Cadenas de texto.|
| 471 | Buscando el pinchazo |Expresiones.|
| 472 | Caminando voy |Condicionales.|
| 616 | [Pic,poc,pic,...pong](https://www.aceptaelreto.com/problem/statement.php?id=616)| Tratamiento de la entrada |
| 617 | [Mensajes en Space Invaders](https://www.aceptaelreto.com/problem/statement.php?id=617) | Tratamiento de la entrada |

## Ejercicios Intermedio

![Easy](imgs/medio-medio.png)\

| Número  | Título  | Temario| 
|---------|---------|--------|
|102| Encriptación de mensajes | Cadenas de texto (String). Vectores.|
|115| Número de Kaprekar | Algoritmo ad-hoc.|
|148| Nochevieja | Expresiones. Tratamiento datos de entrada.|
|154| ¿Cuál es la siguiente matrícula? |Vectores / String.|
|160| Matrices triangulares | Matrices.|
|165| Número hyperpar | Expresiones.|
|173| Partido de squash |Vectores / String.|
|188| Encadenando palabras |Cadenas de texto (String).|
|260| Alan Smithee |Cadenas de texto (String) / Vectores.|
|265| Suma descendente| Expresiones.|
|300| Palabras pentavocálicas| Cadenas de texto (String).|
|309| Cálculo mental | Bucles simples. Cadenas de texto (String).|
|364| Espionaje en Navidad  | Cadenas de texto (String).|
|372| La farsante de Mary Poppins  | Cadenas de texto (String).|
|420| El secreto de la bolsa  | Expresiones.|
|433| Racimos de uvas  | Expresiones / Bucles simples.|
|442| Camellos, serpientes y kebabs  | Cadenas de texto (String).|
|466| Multiplicando mal |  Bucles simples.|
|100 | Constante de Kaprekar  | Algoritmo de Ordenación.|
|121 | Chicles de regalo  | Bucles simples.|
|124 | ¿Cuántas me llevo?  | Recorridos.|
|162 | Tableros de ajedrez  | Bucles simples.|
|167 | Pintando fractales  | Recursividad.|
|263 | El cuadrado de Rubik  | Matrices. Tratamiento datos de entrada.|
|333 | Números bicapicúa  | Bucles simples.|
|334 | Galos, romanos y otras gentes  | Cadenas de texto (String).|
|397 | ¿Es múltiplo de 3?  | Expresiones.|
|415 | Resta y amus  | Expresiones. Pensar.|
|417 | Nana al bebé de papá y mamá |  Cadenas de texto (String).|
|419 | Telesillas  | Algoritmo voraz.|
|428 | Tendencia al lado oscuro |  Expresiones.|
|429 | Organizando los hangares  | Algoritmo voraz.|
|464 | Entrando en pelotón  | Ordenación por varios campos.|
|470 | Montando semáforos  | StringBuilder|
|618 | [Buscando el Nivel ](https://www.aceptaelreto.com/problem/statement.php?id=618) | Vectores y Operaciones| 
|619 | [Pantallas de carga](https://www.aceptaelreto.com/problem/statement.php?id=619) | Matrices | 


## Difíciles

![Easy](imgs/hard-bajo.png)\

| Número  | Título  | Temario| 
|---------|---------|--------|
| 101 | Cuadrados diabólicos y esotéricos  | Matrices.|
| 129 | Marcadores de 7 segmentos  | Matrices.|
| 176 | Campo de minas  | Matrices.|
| 259 | ¿Cuántos números capicúa?  | BigInteger|
| 348 | Distancia al siguiente capicúa  | Algoritmo ad-hoc.|
| 365 | En la cola de Papá Noel | |
| 409 | Seleccionando castellers  | Algoritmo voraz.|
| 414 | Conseguir un cuadrado perfecto| | 
| 430 | Las pruebas del maestro Yoda| | 
| 431 | Genética Jedi| | 
| 445 | Semana de la Informática  | Algoritmo voraz.|
| 469 | Pesando patatas Pensar.| | 
|113 |Semáforos sin parar  | Búsqueda en el espacio de soluciones.|
|146 |Números afortunados| | 
|226 |Reescribiendo fracciones|Imposible| 
|271 |Pájaros en vuelo  | Colas de prioridad.| 
|315 |Jugando al Buscaminas  | Matrices, recursividad.|
|366 |Los niños buenos  | Algoritmo de Ordenación (Shellsort).|
|389 |Aritmética verbal  | Búsqueda exhaustiva, vuelta atrás.|
|412 |Ovejas negras  | Matrices, recursividad.|
|418 |RENUM| | 
|432 |Escapando de las fuerzas imperiales  | Matrices, recursividad.|
|449 |Trim  | Programación dinámica|
|620 | [Juegos en Cintas](https://www.aceptaelreto.com/problem/statement.php?id=620)| ¿Problema de la mochila? |

