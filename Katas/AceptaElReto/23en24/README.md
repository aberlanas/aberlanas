# 23 en 24 

## La vuelta al mundo

El maratón 24 en 23 recorre todos los husos horarios. Los problemas son siempre publicados a las 12 del mediodía pero de husos horarios distintos. Al ir cambiando el huso horario hacia el este daremos la vuelta al mundo ganando una hora diaria. 

Gracias a eso conseguimos recorrer los 24 husos horarios en 23 días.