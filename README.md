# Welcome 👋

My name is Angel Berlanas Vicente, IT Teacher and OpenSource Lover.
In this page you can find some (useful) ideas and resources about **random** and not so **random** projects.

# Dotfiles

- [DotFiles](https://gitlab.com/aberlanas/aberlanas/-/tree/master/dotfiles)

# Repositories for Teaching 

Year 2024-2025

- [ SOM ](https://gitlab.com/aberlanas/SMX-SOM)
- [ ASO ](https://gitlab.com/aberlanas/ASIR-ASO)

# Other Repositories

- [ SOX ](https://gitlab.com/aberlanas/SMX-SOX)
- [ DIW ](https://gitlab.com/aberlanas/DAW-DIW)

# Repositories for Senia cdd

- [ La Senia Custom Debian Distribution ](https://gitlab.com/aberlanas/senia-cdd#readme)
- [ Infraestructure of La Senia ](https://gitlab.com/aberlanas/senia-cdd/blob/master/docs/Infraestructura.md)
- [ PMB and XarxaLlibres ](https://gitlab.com/aberlanas/pmb-xarxallibres-next)
