        urlToImg = objState["CustomDeck"][str(cuenta)]["FaceURL"]
        pprint(" * Downloading "+urlToImg)
        localImageName="unknown"
        if objState["Nickname"] != "":
            localImageName=objState["Nickname"].replace(' ','_')
        localImagePath="imgsTmp/"+localImageName+"_"+str(cuenta)+".jpg"
        urllib.request.urlretrieve(urlToImg,localImagePath)
        
        # Troceando imagenes
        if os.path.exists(localImagePath+"_cards/"):
            shutil.rmtree(localImagePath+"_cards/")
        os.makedirs(localImagePath+"_cards/")
        
        img = Image.open(localImagePath)
        
        orig_ancho=0
        orig_alto=0
        ancho = ancho_carta
        alto = alto_carta
        carta_numero=0
        
        while carta_numero < 70:
        
            box = (orig_ancho,orig_alto,ancho,alto)
            img2 = img.crop(box)
            try:
                img2.save(localImagePath+"_cards/"+str(carta_numero)+".jpg")
                imageisblack = cv2.imread(localImagePath+"_cards/"+str(carta_numero)+".jpg")
                gray_version = cv2.cvtColor(imageisblack, cv2.COLOR_BGR2GRAY)
                if cv2.countNonZero(gray_version) <= 3000:
                    print(" Image seems Black : "+localImagePath+"_cards/"+str(carta_numero)+".jpg")
                    os.remove(localImagePath+"_cards/"+str(carta_numero)+".jpg")
                
            except Exception as e:
                print(" Error : "+str(carta_numero) + " "+str(e))
            
            orig_ancho=orig_ancho+ancho_carta
            ancho = ancho+ancho_carta
            
            carta_numero = carta_numero + 1
            
            if (carta_numero % 10 == 0):

                orig_ancho = 0
                ancho=ancho_carta
                orig_alto = orig_alto+alto_carta
                alto = alto+alto_carta
                
                #boxaux = (orig_ancho,orig_alto,ancho,alto)
                #print(" Siguiente fila"+str(boxaux))
            
            
        
        cuenta = cuenta +1
        # Dirty hack
        if cuenta == 55 : cuenta=56
    item = item + 1

