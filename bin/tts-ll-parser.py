#!/usr/bin/python3

import os
import sys
import json
import shutil
from pprint import pprint
import urllib.request
from PIL import Image
import cv2



ancho_carta=410
alto_carta=585

tmpDir = "/home/aberlanas/tmp/imgsTmp"

print (" * TTS Simple Parser for Lenins Legacy ")

pprint (" + Preparing environment ")

pprint (" + Deleting dir content +"+tmpDir)
shutil.rmtree(tmpDir) 
if not os.path.isdir(tmpDir):
    pprint(" + Creating directory ")
    os.makedirs(tmpDir)


if len(sys.argv) != 1 and os.path.exists(sys.argv[1]):
    print(" [ File ] : "+sys.argv[1])
else:
    print(" [ Error ] : File not found "+ sys.argv[1])
    sys.exit(1)
    
jsonFilePath  = sys.argv[1]
with open(jsonFilePath,'r') as jsonFile:
    jsonData = json.load(jsonFile)
    
#pprint(jsonData)
cuenta=1
pdfs=1
carta=1
item=0
for objState in jsonData["ObjectStates"]:
    
    # Si son Tokens
    if objState["Name"] == "Custom_Token":
        if str(objState["Nickname"]) != "":
            nickname = str(objState["Nickname"])
            print(" * Nickname = "+nickname)
        else:
            nickname=str(cuenta)
            cuenta=cuenta+1
        urlToImg = objState["CustomImage"]["ImageURL"]
        pprint(" * Downloading "+urlToImg)
        localImageName="unknown"
        localImagePath=tmpDir+"/"+nickname+".jpg"
        urllib.request.urlretrieve(urlToImg,localImagePath)
    
    # Si son casillas
    if objState["Name"] == "Custom_Tile":
        if str(objState["Nickname"]) != "":
            nickname = str(objState["Nickname"])
            print(" * Nickname = "+nickname)
        else:
            nickname=str(cuenta)
            cuenta=cuenta+1
        urlToImg = objState["CustomImage"]["ImageURL"]
        pprint(" * Downloading "+urlToImg)
        localImageName="unknown"
        localImagePath=tmpDir+"/"+nickname+".jpg"
        urllib.request.urlretrieve(urlToImg,localImagePath)
    
    #  Si es un PDF
    if objState["Name"] == "Custom_PDF":
        if str(objState["Nickname"]) != "":
            nickname = str(objState["Nickname"])
            print(" * Nickname = "+nickname)
        else:
            nickname=str(pdfs)
            pdfs=pdfs+1
        urlToImg = objState["CustomPDF"]["PDFUrl"]
        pprint(" * Downloading "+urlToImg)
        localImageName="unknown"
        localImagePath=tmpDir+"/pdf_"+nickname+".pdf"
        urllib.request.urlretrieve(urlToImg,localImagePath)
    
    # Si son cartas
    if objState["Name"] == "Deck":
        if str(objState["Nickname"]) != "":
            nickname = str(objState["Nickname"]).replace(" ","_")
            print(" * Deckname = "+nickname)
        else:
            nickname=str(cuenta)
            cuenta=cuenta+1
        for card in objState["CustomDeck"]:
            urlToImg = objState["CustomDeck"][card]["BackURL"]
            pprint(" * Downloading "+urlToImg)
            localImageName="unknown"
            localImagePath=tmpDir+"/carta_back_"+nickname+".jpg"
            urllib.request.urlretrieve(urlToImg,localImagePath)
            
            urlToImg = objState["CustomDeck"][card]["FaceURL"]
            pprint(" * Downloading "+urlToImg)
            localImageName="unknown"
            localImagePath=tmpDir+"/carta"+str(carta)+"_"+nickname+".jpg"
            urllib.request.urlretrieve(urlToImg,localImagePath)
            carta = carta+1
