;;
;; Angel Berlanas Emacs config
;; Turn off unwanted GUI elements.
(menu-bar-mode 1)
(scroll-bar-mode -1)
(tool-bar-mode 1)
(tooltip-mode 1)

;; Set the default font.
;; Append a size -NN to the font name.
;;(add-to-list 'default-frame-alist
;;             '(font . "Cantarell-15"))

;;(load-theme 'zenburn t)
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
(load-theme 'zenburn t)


(electric-pair-mode 1)
(electric-indent-mode -1)
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)
(setq-default indent-line-function 'insert-tab)

(setq backup-directory-alist '(("." . "~/.saves")))
(setq backup-by-copying t)
(setq version-control t)
(setq delete-old-versions t)
(setq create-lockfiles nil)

(setq inhibit-startup-message t)
(setq message-log-max nil)
(kill-buffer "*Messages*")

(setq user-full-name "Angel Berlanas Vicente")
(setq user-mail-address "a.berlanasvicente@edu.gva.es")

(setq browse-url-generic-program (executable-find "/usr/bin/firefox"))
(setq browse-url-browser-function 'browse-url-generic)
